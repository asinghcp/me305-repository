'''
@file lab7PCUI.py
@author Anil Singh
@brief This file contains the Spyder User Interface to read data from a Nucleo
@details This file contains the Spyder User Interface to read data from a
         Nucleo. The user can specify a Kp Gain value. The UI will plot the
         motor velocity and position response against the given reference 
         profile, displaying the desired Kp value and calculated J value.
'''

import serial as cereal
import time as thyme
import array
import ast
# import numpy as jacob
from matplotlib import pyplot as kai

ser = cereal.Serial(port='COM6',baudrate=115273,timeout=1)
#sets baudrate, COM port, and timeout


class UserInterface:
    '''
    User interface task.
    
    An object of this class runs the UI. User inputs desired proportionality
    constant for the closed loop.
    '''
    ## Initialization state
    S0_INIT          = 0
    
    ## Data Processing state
    S1_Data          = 1
    
    ## Finalization State
    S2_Final         = 2

    def __init__(self, interval):
        '''
        Creates a UI task object.
        @param interval Microseconds between runs of task
        '''
    
        ##  Port
        self.ser = ser       
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        #self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = thyme.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
                
        ## Kp
        self.Kp = input('Input Gain Value Kp: ')
        
        ## preallocates reference Data arrays
        self.t_ref   = []
        self.v_ref   = []
        self.x_ref   = []
        self.t_trunc = []
        self.v_trunc = []
        self.x_trunc = []
        
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = thyme.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                ## Run State 0 code
                if float(self.Kp) <= 0:
                    self.Kp = input('Input a positive, nonzero Gain Value Kp: ')   
                
                print('Calculating Motor Response to Reference Data Profile...')
                ## Write control parameters                                 
                kp_trans = str(self.Kp)+'\n'
                self.ser.write(kp_trans.encode())

                ## Transition to state 1
                self.transitionTo(self.S1_Data)
            
            ## Read and Plot Data
            if(self.state == self.S1_Data):
                ## Run state 1 code
                if self.ser.in_waiting != 0:
                    
                    # Read Data
                    J = ser.readline().decode('ascii')
                    
                    '''
                    data = list(ser.readline().decode('ascii').split('MM'))
                    data_out = [m.strip('[]').split(', ') for m in data]
                    t_stamp = [float(t) for t in data_out[0]]
                    v_act   = [float(t) for t in data_out[1]]
                    x_act   = [float(t) for t in data_out[2]]  
                    '''
                    
                    t_stamp = list(ser.readline().decode('ascii').strip("array('f', ").strip(", '\n'").split(')'))
                    v_act   = ser.readline().decode('ascii').strip("array('f', ").split(')')
                    x_act   = ser.readline().decode('ascii').strip("array('f', ").split(')')

                    v_trunc = ser.readline().decode('ascii').strip("array('f', ").split(')')   
                    x_trunc = ser.readline().decode('ascii').strip("array('f', ").split(')')
                                        
                    ## Plotting Data
                    ## Velocity
                    fig, axes = kai.subplots(2)
                    fig.suptitle('Motor Response vs Reference Profile with Kp = '+str(self.Kp)+' and J = '+str(J))
                    kai.subplot(2,1,1)
                    kai.plot(t_stamp, v_act, 'b', t_stamp, v_trunc, 'g--')
                    kai.xlabel('Time (s)')
                    kai.ylabel('Motor Velocity (RPM)')
                    
                    ## Position
                    kai.subplot(2,1,2)
                    kai.plot(t_stamp, x_act, 'b', t_stamp, x_trunc, 'g--')                  
                    kai.xlabel('Time (s)')
                    kai.ylabel('Motor Position (Degrees)')
                    
                    self.transitionTo(self.S2_Final)
          
            elif(self.state == self.S2_Final):
                quit()
                    
            # Specifying the next time the task will run
            self.next_time += self.interval                    


    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState New state object assumes
        '''
        self.state = newState                                        