'''
@file    Lab07CombinedControl.py
@brief   This file contains the combined Execution and Control Tasks for Lab 7
@details This file combines the previously separated ExecTask and TaskControl
         due to an inability to solve an encoding issue. The purpose of this
         combination is to bypass a shares file and implement everything in
         one file. Here, the csv refrence file is imported, processed, and 
         used to calculate J. processed values are written to the UI for 
         plotting.
@author  Anil Singh
'''

import pyb
import utime
import array
from pyb import UART
from lab7Encoder import Encoder
from Lab07motordriver import MotorDriver
from lab7closedloop import ClosedLoop

class CombinedControl:
    
    ## Initialization state
    S0_INIT          = 0
    
    ## Control state
    S1_Control       = 1 

    ## Finalization State
    S2_Final         = 2
    
    ## Poggers State
    S3_Poggers       = 3
    
    def __init__(self,interval):
        '''
        This class coordinates the inputs from \ref lab6PCUI.py to control the 
        motor system through the Nucleo. The class calls the closed loop file and 
        encoder data, and sends the PWM control scheme as an output.
        '''
        ##  The amount of time in microseconds between runs of the task
        self.interval = interval
        
        ## Timing 
        ##  Serial Port communication
        self.serial = UART(2)
        ## first iteration
        self.start_time = utime.ticks_us()
        ## next iteration timestamp
        self.next_time = utime.ticks_add(self.start_time, self.interval)
            
        ## Initial State
        self.state = self.S0_INIT
        
        ## Store copies of classes from imported modules
        self.Encoder = Encoder('B6', 'B7', 4) ## Encoder pins
        self.MotorDriver = MotorDriver('A15', 'B4', 'B5', 3) ## Motor Pins
        
        ## Enable Motor Driver
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        self.ClosedLoop = ClosedLoop
        
        ## Class Copy array
        self.array = array
        
        ## Empty arrays:
        self.t       = self.array.array('f', [0])
        self.x       = self.array.array('f', [0])
        self.v       = self.array.array('f', [0])
        self.J_array = self.array.array('f', [0])
        #self.t_ref   = self.array.array('f', [0])
        #self.v_ref   = self.array.array('f', [0])
        #self.x_ref   = self.array.array('f', [0])
        self.t_trunc = self.array.array('f', [0])
        self.v_trunc = self.array.array('f', [0])
        self.x_trunc = self.array.array('f', [0])
    
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        #self.serial.write('Test'.encode('ascii'))
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            ## Update Encoder
            self.Encoder.update()
            
            if self.state == self.S0_INIT:
                
                if self.serial.any() != 0:
                ## Read data in serial port
                    self.Kp = self.serial.readline()
                    
                    self.ClosedLoop = ClosedLoop(float(self.Kp), 100, -100) ## Kp and Max and Min PWM Limit
                    
                    ref_data = open('lab7_reference.csv')
                    while True:
                        line = ref_data.readline()
                        if line == '':
                            break
                        else:
                            (t,v,x) = line.strip().split(',');
                            if float(t)%0.02 < 5E-4:
                                self.t_trunc.append(float(t))
                                self.v_trunc.append(float(v))
                                self.x_trunc.append(float(x))
                                
                        line = None
                    
                   # i = 0
                   # while i <= 15000:
                        #self.t_trunc.append(self.t_ref[i]) 
                        #self.v_trunc.append(self.v_ref[i]) 
                        #self.x_trunc.append(self.x_ref[i])
                        #i += 20    
                
                    ## Transition to state 1
                    self.transitionTo(self.S1_Control)
                    self.runs = 0
                    

            if self.state == self.S1_Control:
                

                if len(self.t) <= len(self.t_trunc):

                    ## Read Encoder Value, convert to revolutions per minute (RPM)
                    ## 4*1000CPR
                    PPR = 4000
                    interval_change = self.Encoder.get_delta()/PPR
                    divider1 = self.interval*1E-6
                    divider2 = divider1/60
                    velocity_actual = interval_change/divider2
                    
                    PPD = PPR/360 
                    ## Pulses per degree
                    p_act = self.Encoder.get_position()/PPD 
                    ## Converts encoder position to position in degrees
                    
                    ## Write/append data 
                    self.v.append(velocity_actual)                    
                    self.t.append(self.t_trunc[self.runs])
                    self.x.append(p_act)
                                        
                    ## Control Loop Gain Algebra
                    L = self.ClosedLoop.update(0.5, float(self.v_trunc[self.runs]), velocity_actual, self.x_trunc[self.runs], p_act )
                    velocity_actual = None
                    p_act = None
                    
                    ## Set Motor Driver Output to L
                    self.MotorDriver.set_duty(L)
                    
                    
                    ## Calculate Individual J values
                    
                    vel_diff = self.v_trunc[self.runs] - self.v[self.runs]
                    pos_diff = self.x_trunc[self.runs] - self.x[self.runs]
                    J = vel_diff**2 + pos_diff**2
                    self.J_array.append(J) 
                    
                    ## Increment run count
                    self.runs += 1
                                        
                else:
                    self.transitionTo(self.S2_Final)
                    
            if self.state == self.S2_Final:
                ## Duty Cycle set to 0 for all other cases other than desired
                    self.MotorDriver.set_duty(0)
                    
                    
                    ## Sum individual J values, divide by length of J
                    
                    J = sum(self.J_array)/len(self.J_array)
                    self.serial.write(str(J) + '\n')                    
                    self.serial.write(str(self.t)+'\n')
                    self.serial.write(str(self.v)+'\n')
                    self.serial.write(str(self.x)+'\n')

                    self.serial.write(str(self.v_trunc)+'\n')
                    self.serial.write(str(self.x_trunc)+'\n')
                    
                    self.transitionTo(self.S3_Poggers)
            
            if self.state == self.S3_Poggers:
                pass
                    
            else:
                pass
                
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    