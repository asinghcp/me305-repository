'''
@file       main.py
@brief      Nucleo executes this file automatically on startup without the need for 'execfile'
@details    Calls the CombinedControl, Motor Driver, Closed Loop, and Encoder, 
            without using the shares file to coordinate variable
            transmission between tasks.
@author     Anil Singh
'''

import utime
import pyb

from pyb import UART
from lab7Encoder import Encoder
from Lab07motordriver import MotorDriver
from lab7closedloop import ClosedLoop
from Lab07CombinedControl import CombinedControl

task = CombinedControl(2_0000)

while True:
    task.run()