'''
@file Lab0x02task2virtual.py

This file serves as an implementation of a finite-state-machine using
Python. The example will implement some code to pulse a physical LED, changing
the brightness to follow a Sine Wave and Triangle Wave. This code is adapted
to run a virtual LED in Spyder.The output provides the current run, state,
waveform, brightness percentage, and timestamp. 

@author Anil Singh

'''
import time
import math

class TaskLEDControlV:
    '''
    @brief      A finite state machine to control a blinking LED.
    @details    This class implements a finite state machine to control the
                operation of a pulsing LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                   = 0    
    
    ## Constant defining State 1
    S1_SineWave           = 1    
    
    ## Constant defining State 2
    S2_TriangleWave             = 2    
     
    def __init__(self, interval):
        '''
        @brief            Creates a TaskLEDControl object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''

        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print(str(self.runs) + ' Startup ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_SineWave)
               
            elif(self.state == self.S1_SineWave):
                # Run State 1 Code
                print(str(self.runs) + ': State 1: ' + 'Sine Wave ' + str(50*(math.sin((self.curr_time-self.start_time)*(2*math.pi/10)))+50) + ', ' + str(self.curr_time-self.start_time))
                if((self.curr_time-self.start_time) >= 30):
                    self.transitionTo(self.S2_TriangleWave)
            
            elif(self.state == self.S2_TriangleWave):
                # Run State 2 Code
                print(str(self.runs) + ': State 2: ' + 'Triangle Wave ' + str(10*((self.curr_time-self.start_time) % 10)) + ', ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
        