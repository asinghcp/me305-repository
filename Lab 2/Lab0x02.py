'''
@file Lab0x02.py

This file serves as an implementation of a finite-state-machine using
Python. The example will implement some code to blink a Virtual LED on and
off

@author Anil Singh

'''
import time

class TaskLEDControl:
    '''
    @brief      A finite state machine to control a blinking LED.
    @details    This class implements a finite state machine to control the
                operation of a blinking LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                   = 0    
    
    ## Constant defining State 1
    S1_LEDon           = 1    
    
    ## Constant defining State 2
    S2_LEDoff              = 2    
     
    def __init__(self, interval):
        '''
        @brief            Creates a TaskLEDControl object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''

        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print(str(self.runs) + ' Startup ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_LEDon)
               
            elif(self.state == self.S1_LEDon):
                # Run State 1 Code
                print(str(self.runs) + ' LED on ' + str(self.curr_time-self.start_time))   
                self.transitionTo(self.S2_LEDoff)
            
            elif(self.state == self.S2_LEDoff):
                # Run State 2 Code
                print(str(self.runs) + ' LED off ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_LEDon)
                 
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
        