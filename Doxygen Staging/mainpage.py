'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
This website hosts documentation related to Anil Singh's ME305 repository located 
at:
    https://bitbucket.org/asinghcp/me305-repository/src/master/
    
@image html nucleo.png width=600in

@section sec_references References
The documentation and code displayed at this site was developed in conference
with the following engineers:\n
    --Kai Quizon, EIT (https://kquizon.bitbucket.io)\n
    --Jacob Winkler (https://jcwinkle.bitbucket.io)\n
    
The included code and documentation would be impossible without the help and
guidance of:\n
    --Charlie Refvem, Lecturer, Cal Poly SLO\n
    --Dr. Eric Espinoza-Wade, Professor, Cal Poly SLO\n
    
@section sec_directory Directory
1. \ref page_fib
2. \ref page_StateSys
3. \ref page_LEDcontrol
4. \ref page_EncoderControl
5. \ref page_EncoderDataCollection
6. \ref page_BLuetoothLEDControl
7. \ref page_CLFMotorController
8. \ref page_ReferenceTracking

@page page_fib Fibbonaci (Lab 1)

@section page_fib_desc Description
The code on this page documents a program housed within Spyder to calculate 
a Fibbonaci number at a given index. The user is able to provide the desired
index in the command prompt and the corresponding Fibbonaci number is 
returned. In addition, the user is able to specify the first Fibbonaci 
number returned by entering it into line 49.

@image html fib_gif.gif

@section page_fib_src Source Code Access
Source Code related to Lab 1 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%201/Lab1fib.py

@section page_fib_doc Documentation
Documentation related to Lab 1 is located at \ref Lab1fib.py

@page page_StateSys Elevator State System (HW0)

@section page_ss_desc Description
The elevator state system source code allows for the operation of two
independant elevator system operating between two floor. The user can provide
input through floor buttons located on each floor. In addition, each floor
has a proximity sensor that indicates the presence of the elevator on that
floor. Finally, there is a single motor capable of driving the elevator up
or down in the system. The state diagram is provided below.

@image html elevatordiagram.png

@image html statesysdiagram.png

@section page_ss_src Source Code Access
Source Code related to HW0 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Homework0x00/Homework0x00.py

@section page_ss_doc Documentation
Documentation related to HW0 is located at \ref Homework0x00.py \n
Documentation related to the mainscript is located at \ref main_Homework0x00.py

@page page_LEDcontrol LED Control (Lab 2)

@section page_LEDcontrol_desc Description
The code on this page documents a program that runs two tasks on a Nucleo L476
board. One task controls the operation of a "virtual" LED, commanding it to 
turn off and on at a specified interval. The other task controls the physical 
LEDs on the Nucleo, commanding them to pulse at a desired interval and pattern
using pulse width modulation.

@section page_LEDcontrol_ss_diagram State System Diagrams

@image html lab2virtual.png

@image html lab2physical.png

@section page_LEDcontrol_src Source Code Access
Source Code related to Lab 2 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%202/

@section page_LEDcontrol_doc Documentation
Documentation related to the LED ON/OFF function is located at \ref Lab0x02.py \n
Documentation related to the virtual LED pulse function is located at \ref Lab0x02task2virtual.py \n
Documentation related to the physical LED pulse function is located at \ref Lab02Physical.py \n
Documentation related to the mainscript is located at \ref Lab02main.py

@page page_EncoderControl Encoder Control (Lab 3)

@section page_EncoderControl_desc Description
The code on this page documents a program that reads the position of a motor
encoder given the specified timer and pins. Included is an Encoder class that
sets up the timer and pins to run the Encoder, as well as a Finite State
Machine that updates the position of the encoder and accepts commands from 
the user with a Spyder based UI. The tasks are run together using a shares 
file. \n
\n
The user is able to print encoder position, zero encoder position, print the
delta of the current encoder position, and specifcy a time interval. \n
\n
The Encoder class was updated for use in labs 6 and 7. As such, the following
files represent a more refined apprach to the Encoder class: \n
\ref lab6Encoder.py \n
\ref lab7Encoder.py

@image html encoder_gif.gif

@section page_EncoderControl_ss State System Diagram

@image html lab3user.png

@image html lab3encoder.png

@section page_EncoderControl_td Task Diagram

@image html lab3td.png

@section page_EncoderControl_src Source Code Access
Source Code related to Lab 3 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%203/

@section page_EncoderControl_doc Documentation
Documentation related to the Encoder Class function is located at \ref Encoder.py \n
Documentation related to the Encoder Task function is located at \ref Lab03encoder.py \n
Documentation related to the User Interface Task function is located at \ref Lab03user.py \n
Documentation related to the shares function is located at \ref Lab03shares.py \n
Documentation related to the mainscript is located at \ref Lab03main.py

@page page_EncoderDataCollection Encoder Data Collection (Lab 4)

@section page_EncoderDataCollection_desc Description
The code on this page documents a program that utilizes motor encoders to 
plot the positional response of the encoder over time. The program uses 
a UI that runs in Spyder to communicate and command a set of files on the
Nucleo over the serial port. The Nucleo processes the data, and the UI sends
user commands to the nucleo and creates a CSV file of collected data, as well
as plots the response.

@section page_EncoderDataCollection_ss State System Diagrams
The PC User Interface runs on one finite state state machine controled by a 
mainscript. The Nucleo runs an Encoder task controled by its own mainscript.

@image html lab4user.png

@image html lab4encoder.png 

@section page_EncoderDataCollection_td Task Diagrams
    
@image html lab4td.png

@section page_EncoderDataCollection_src Source Code Access
Source Code related to Lab 4 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%204/

@section page_EncoderDataCollection_doc Documentation
Documentation related to the Encoder Class function is located at \ref Encoder.py \n
Documentation related to the Encoder Task function is located at \ref Lab04encoder.py \n
Documentation related to the User Interface Task function is located at \ref lab4UI.py \n
Documentation related to the UI mainscript is located at \ref Lab04UImain.py \n
Documentation related to the updated mainscript is located at \ref main.py

@page page_BLuetoothLEDControl Bluetooth LED Control (Lab 5)

@image html bt_gif.gif width=600in

@section page_BLuetoothLEDControl_desc Description
The code on this page documents a program that utilizes a bluetooth antenna
to control the blinking of the LEDs on the Nucleo. The user has the following
options: \n
1) Turn LED on \n
2) Turn LED off \n
3) Specify Blinking Frequency for LED \n

@section page_BLuetoothLEDControl_tech Hardware
Bluetooth Antenna: DSD TECH HC-05 Bluetooth Serial Pass-through Module 
Wireless Serial Communication with Button for Arduino \n
https://www.amazon.com/DSD-TECH-HC-05-Pass-through-Communication/dp/B01G9KSAF6/ref=sxts_sxwds-bia-wc-p13n1_0?cv_ct_cx=bluetooth&dchild=1&keywords=bluetooth&pd_rd_i=B01G9KSAF6&pd_rd_r=1b8d1351-8fcc-4567-95aa-4b0c4a720679&pd_rd_w=gubkN&pd_rd_wg=wAusB&pf_rd_p=1835a2a9-7ed8-48dc-ad07-fcd7527bd2bc&pf_rd_r=FQ88X62BFYQ521PY3E25&psc=1&qid=1607144230&sr=1-1-80ba0e26-a1cd-4e7b-87a0-a2ffae3a273c

@section page_BLuetoothLEDControl_src Source Code Access
Source Code related to Lab 5 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%205/

@section page_BLuetoothLEDControl_doc Documentation
Documentation related to the LED Control Task function is located at \ref lab05FSM.py \n
Documentation related to the mainscript is located at \ref lab05main.py

@section page_lab5ss_desc Lab 5 State System Diagram

@image html la5statesysdiagram.png

@page page_CLFMotorController Closed Loop Feedback Motor Controller (Lab 6)

@section page_CLFMotorController_desc Description
The code on this page documents a program that controls a motor using a 
proportional control feedback loop. A user interfaced housed in Spyder 
accepts user inputs for gain (Kp) and uses drivers on the Nucleo to obtain
a step response from the motor. The response is then returned the the user
interface and plotted against the given reference velocity as a metric of
performance. 

@section page_CLFMotorController_desc Gain Constant Refinement
The issue experienced in lab 6 was determined through many many office hours
and debugging to be in the Encoder class. This is why in lab 3 and 4, links
are provided to the updated encoder classes in lab 6 and 7. However, despite
this refinement, there is still an unknown problem in the encoder class. Below
is an image of the plot obtained from the program.

@image html lab6response.png
 
The issues with this plot lie in the scaling of the magnitude and the negative
nature of the response. Varying Kp has the following effect on the response.

@image html lab6kp0.5.png
@image html lab6kp0.1.png
@image html lab6kp0.06.png
@image html lab6kp0.04.png
@image html lab6kp0.03.png

Notice how there is very little effect for Kp on the plot structure. This is
likely due to the magnitude scaling. Kp does have an effect on the response
though, as seen in Kp=0.03. Here, the motors did not spin, which outputted the
anticipated response of a motor velocity of 0rpm. 

@section page_CLFMotorController_debug Debugging Attempts
As states earlier, the issue with this response was narrowed down to the 
encoder class. This was done by creating a seperate file \ref debug_file.py
designed to isolate the encoder class from the math occuring in the main
program. The debug file only utilized the motor driver (which was previously 
tested for any bugs and deemed functioning) and the encoder. The motor was
driven with pwm and the encoder response was observed. A negative response
was observed when a positive one should have been experienced, confirming
that the encoder was in fact the issue. Furthermore, Professor Wade ran the
encoder on his hardware and experienced the same issue. Interestingly,
however, Jacob Winkler and Kai Quizon's were able to use almost identical
encoder classes with their programs and produce expected velocity responses. 
(Links to their responses can be found below) \n
Kai's Response - https://kquizon.bitbucket.io/page_MotorControl.html \n
Jacob's Response - https://jcwinkle.bitbucket.io/page_lab6w2.html \n
Although we have code that was developed in conference, using their encoders
in my debug file and in my program yielded identical errors. Professor Wade
and I were unable to determine the source of this encoder error. Below are the
results of the debug file for Professor Wade's reference.

@image html lab6debug.png

@section page_CLFMotorController_ss Combined Control State System Diagram

@image html lab6ss.png

@section page_CLFMotorController_td Task Diagram
    
@image html lab6td.png
   
@section page_CLFMotorController_src Source Code Access
Source Code related to Lab 6 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%206/

@section page_CLFMotorController_doc Documentation
Documentation related to the Combined Control file is located at \ref Lab06CombinedControl.py \n
Documentation related to the motor driver file is located at \ref Lab06motordriver.py \n
Documentation related to the closed loop file is located at \ref lab6closedloop.py \n
Documentation related to the Encoder file is located at \ref lab6Encoder.py \n
Documentation related to the User Interface frontend is located at \ref lab6PCUI.py \n
Documentation related to the mainscript for the UI frontend is located at \ref Lab06UImain.py \n
Documentation related to the mainscript on the nucleo is located at \ref main.py \n

@page page_ReferenceTracking Closed Loop Feedback Motor Controller - Reference Tracking (Lab 7)

@section page_ReferenceTracking_desc Description
The code on this page documents a program that controls a motor using a 
proportional control feedback loop. A user interfaced housed in Spyder 
accepts user inputs for gain (Kp) and uses drivers on the Nucleo to obtain
a response from the motor. The response is similar to that of lab 7, however, 
the motor response occurs in reference to a set of reference data. In this 
way, the motor "tracks" a reference profile and attempts to minimize the error 
between the reference profile and the actual response of the motor. The 
relationship between the two is outputted as a subplot illustrating velocity 
response and position response in comparison to their reference profiles,
respectively. 

@section page_ReferenceTracking_result Results and Debugging
There were three large errors that contributed to the errors in response from
the hardware. The first lied in interpreting data properly for plotting.
Characters tacked on to the end of the strings were unable to be stripped 
properly, resulting in an inability to plot the actual response of the system.
The second issue lied in the timing of the system. The program runs for
about 20 seconds, in which the motors only attempt to physically track the 
reference profile (As in physically spin) for the last second. Print
statements revealed that there were problems with the length of the time
span the program runtime was based off of. The third issue lied in the 
processing of the reference CSV file. The raw CSV file is loaded on to the 
nucleo and proccessed from the nucleo side. Although there should have been
around 750 data points post processing, there were only around 590. Below is
the output plot from the hardware. \n

@image html hwresponse.png "Hardware Response"

Although we were unable to obtain a response from the hardware directly,
a simulated motor response was obtained. The randomness of the motor response
was captured using a random number generator. This was adjusted to simulate
and resemble the control reference profile, although some of the profile was 
interpreted as a sharp change instead of a curve. \n

@image html simresponse.png "Simulated Response"

This simulation represents the desired output of the system given more time to 
debug the posted code. The offset was generated using a data analysis from 
Lab 6 refined Kp plots, with reference to Kai Quizon and Jacob Winkler's lab 
6 responses given that my lab 6 experienced issues from the encoder. Their 
results are linked below. Finally, the randomness in data was analyzed using 
an IQR (Inter-Quartile Range) analysis at steady state RPM from lab 6. The 
ramping of the motor was unable to be simulated. \n
Kai's Response - https://kquizon.bitbucket.io/page_velprofile.html \n
Jacob's Response - https://jcwinkle.bitbucket.io/page_lab7.html

@section page_ReferenceTracking_ss State System Diagrams

@image html lab7user.png

@image html lab7nucleo.png

@section page_ReferenceTracking_td Task Diagram
    
@image html lab7td.png

@section page_ReferenceTracking_src Source Code Access
Source Code related to Lab 7 is located at: https://bitbucket.org/asinghcp/me305-repository/src/master/Lab%207/

@section page_ReferenceTracking_doc Documentation
Documentation related to the Combined Control file is located at \ref Lab07CombinedControl.py \n
Documentation related to the motor driver file is located at \ref Lab07motordriver.py \n
Documentation related to the closed loop file is located at \ref lab7closedloop.py \n
Documentation related to the Encoder file is located at \ref lab7Encoder.py \n
Documentation related to the User Interface frontend is located at \ref lab7PCUI.py \n
Documentation related to the mainscript for the UI frontend is located at \ref Lab07UImain.py \n
Documentation related to the mainscript on the nucleo is located at \ref main.py \n
'''
