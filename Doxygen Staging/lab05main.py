'''
@file Lab05main.py
@author Anil Singh
@brief main file to coordinate the finite state machine for lab 5
'''

from lab05FSM import Lab5LEDControl

task = Lab5LEDControl(100)

while True:
    task.run()