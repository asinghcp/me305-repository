var classLab06CombinedControl_1_1CombinedControl =
[
    [ "__init__", "classLab06CombinedControl_1_1CombinedControl.html#ac6185b67ac7f43d7950dac554d40bfa3", null ],
    [ "run", "classLab06CombinedControl_1_1CombinedControl.html#a1d1ccadc8507b413c6134a8eff201a16", null ],
    [ "transitionTo", "classLab06CombinedControl_1_1CombinedControl.html#ac141de03bfff98b81f32c52aeb1a1087", null ],
    [ "ClosedLoop", "classLab06CombinedControl_1_1CombinedControl.html#aaa366fc82dead97ca5054631cd79a45f", null ],
    [ "curr_time", "classLab06CombinedControl_1_1CombinedControl.html#a8b9ff6052148b77c89f72a8088edc3eb", null ],
    [ "Encoder", "classLab06CombinedControl_1_1CombinedControl.html#abc4b5c36b814cf63c9dd06de5eb23c44", null ],
    [ "interval", "classLab06CombinedControl_1_1CombinedControl.html#a51f903583ea48adf39c305dcc1c186b3", null ],
    [ "Kp", "classLab06CombinedControl_1_1CombinedControl.html#a8c59756123b1dc69a3063a605b656f87", null ],
    [ "MotorDriver", "classLab06CombinedControl_1_1CombinedControl.html#aba56c9b3e40fbf0c363f9d7f17e129a6", null ],
    [ "next_time", "classLab06CombinedControl_1_1CombinedControl.html#a73afe48187198d50e8c8a4c495cca68d", null ],
    [ "ref", "classLab06CombinedControl_1_1CombinedControl.html#a22a1f405ed9b9fb1ec7a3b40a828d27c", null ],
    [ "ref_vel", "classLab06CombinedControl_1_1CombinedControl.html#a3ad75d0240c40f3def091abb36f5dd83", null ],
    [ "serial", "classLab06CombinedControl_1_1CombinedControl.html#a5ee6e7a9c16757fd3c1d35b2a356bb7c", null ],
    [ "start_time", "classLab06CombinedControl_1_1CombinedControl.html#a2a7d5a679e2c414929ebb12537df3e8e", null ],
    [ "state", "classLab06CombinedControl_1_1CombinedControl.html#ace11580949e9be0886ab5e2311ebb245", null ],
    [ "time", "classLab06CombinedControl_1_1CombinedControl.html#a7c2f994f0d3e781fadc9d12308af49e7", null ],
    [ "velocity", "classLab06CombinedControl_1_1CombinedControl.html#ae819667039a01dd20bc3c2732093dab9", null ]
];