/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "References", "index.html#sec_references", null ],
    [ "Directory", "index.html#sec_directory", null ],
    [ "Fibbonaci (Lab 1)", "page_fib.html", [
      [ "Description", "page_fib.html#page_fib_desc", null ],
      [ "Source Code Access", "page_fib.html#page_fib_src", null ],
      [ "Documentation", "page_fib.html#page_fib_doc", null ]
    ] ],
    [ "Elevator State System (HW0)", "page_StateSys.html", [
      [ "Description", "page_StateSys.html#page_ss_desc", null ],
      [ "Source Code Access", "page_StateSys.html#page_ss_src", null ],
      [ "Documentation", "page_StateSys.html#page_ss_doc", null ]
    ] ],
    [ "LED Control (Lab 2)", "page_LEDcontrol.html", [
      [ "Description", "page_LEDcontrol.html#page_LEDcontrol_desc", null ],
      [ "State System Diagrams", "page_LEDcontrol.html#page_LEDcontrol_ss_diagram", null ],
      [ "Source Code Access", "page_LEDcontrol.html#page_LEDcontrol_src", null ],
      [ "Documentation", "page_LEDcontrol.html#page_LEDcontrol_doc", null ]
    ] ],
    [ "Encoder Control (Lab 3)", "page_EncoderControl.html", [
      [ "Description", "page_EncoderControl.html#page_EncoderControl_desc", null ],
      [ "State System Diagram", "page_EncoderControl.html#page_EncoderControl_ss", null ],
      [ "Task Diagram", "page_EncoderControl.html#page_EncoderControl_td", null ],
      [ "Source Code Access", "page_EncoderControl.html#page_EncoderControl_src", null ],
      [ "Documentation", "page_EncoderControl.html#page_EncoderControl_doc", null ]
    ] ],
    [ "Encoder Data Collection (Lab 4)", "page_EncoderDataCollection.html", [
      [ "Description", "page_EncoderDataCollection.html#page_EncoderDataCollection_desc", null ],
      [ "State System Diagrams", "page_EncoderDataCollection.html#page_EncoderDataCollection_ss", null ],
      [ "Task Diagrams", "page_EncoderDataCollection.html#page_EncoderDataCollection_td", null ],
      [ "Source Code Access", "page_EncoderDataCollection.html#page_EncoderDataCollection_src", null ],
      [ "Documentation", "page_EncoderDataCollection.html#page_EncoderDataCollection_doc", null ]
    ] ],
    [ "Bluetooth LED Control (Lab 5)", "page_BLuetoothLEDControl.html", [
      [ "Description", "page_BLuetoothLEDControl.html#page_BLuetoothLEDControl_desc", null ],
      [ "Hardware", "page_BLuetoothLEDControl.html#page_BLuetoothLEDControl_tech", null ],
      [ "Source Code Access", "page_BLuetoothLEDControl.html#page_BLuetoothLEDControl_src", null ],
      [ "Documentation", "page_BLuetoothLEDControl.html#page_BLuetoothLEDControl_doc", null ],
      [ "Lab 5 State System Diagram", "page_BLuetoothLEDControl.html#page_lab5ss_desc", null ]
    ] ],
    [ "Closed Loop Feedback Motor Controller (Lab 6)", "page_CLFMotorController.html", [
      [ "Description", "page_CLFMotorController.html#page_CLFMotorController_desc", null ],
      [ "Debugging Attempts", "page_CLFMotorController.html#page_CLFMotorController_debug", null ],
      [ "Combined Control State System Diagram", "page_CLFMotorController.html#page_CLFMotorController_ss", null ],
      [ "Task Diagram", "page_CLFMotorController.html#page_CLFMotorController_td", null ],
      [ "Source Code Access", "page_CLFMotorController.html#page_CLFMotorController_src", null ],
      [ "Documentation", "page_CLFMotorController.html#page_CLFMotorController_doc", null ]
    ] ],
    [ "Closed Loop Feedback Motor Controller - Reference Tracking (Lab 7)", "page_ReferenceTracking.html", [
      [ "Description", "page_ReferenceTracking.html#page_ReferenceTracking_desc", null ],
      [ "Results and Debugging", "page_ReferenceTracking.html#page_ReferenceTracking_result", null ],
      [ "State System Diagrams", "page_ReferenceTracking.html#page_ReferenceTracking_ss", null ],
      [ "Task Diagram", "page_ReferenceTracking.html#page_ReferenceTracking_td", null ],
      [ "Source Code Access", "page_ReferenceTracking.html#page_ReferenceTracking_src", null ],
      [ "Documentation", "page_ReferenceTracking.html#page_ReferenceTracking_doc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Encoder_8py.html",
"classLab6ExecTask_1_1Lab06ExecTask.html#aeb22448640cbb21b6a950f31b8ca018a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';