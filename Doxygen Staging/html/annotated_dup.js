var annotated_dup =
[
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Homework0x00", null, [
      [ "Button", "classHomework0x00_1_1Button.html", "classHomework0x00_1_1Button" ],
      [ "MotorDriver", "classHomework0x00_1_1MotorDriver.html", "classHomework0x00_1_1MotorDriver" ],
      [ "TaskElevator", "classHomework0x00_1_1TaskElevator.html", "classHomework0x00_1_1TaskElevator" ]
    ] ],
    [ "Lab02Physical", null, [
      [ "TaskLEDControlPhysical", "classLab02Physical_1_1TaskLEDControlPhysical.html", "classLab02Physical_1_1TaskLEDControlPhysical" ]
    ] ],
    [ "Lab03encoder", null, [
      [ "TaskEncoder", "classLab03encoder_1_1TaskEncoder.html", "classLab03encoder_1_1TaskEncoder" ]
    ] ],
    [ "Lab03user", null, [
      [ "TaskUser", "classLab03user_1_1TaskUser.html", "classLab03user_1_1TaskUser" ]
    ] ],
    [ "Lab04encoder", null, [
      [ "Lab4TaskEncoder", "classLab04encoder_1_1Lab4TaskEncoder.html", "classLab04encoder_1_1Lab4TaskEncoder" ]
    ] ],
    [ "lab05FSM", null, [
      [ "Lab5LEDControl", "classlab05FSM_1_1Lab5LEDControl.html", "classlab05FSM_1_1Lab5LEDControl" ]
    ] ],
    [ "Lab06CombinedControl", null, [
      [ "CombinedControl", "classLab06CombinedControl_1_1CombinedControl.html", "classLab06CombinedControl_1_1CombinedControl" ]
    ] ],
    [ "Lab06motordriver", null, [
      [ "MotorDriver", "classLab06motordriver_1_1MotorDriver.html", "classLab06motordriver_1_1MotorDriver" ]
    ] ],
    [ "Lab07CombinedControl", null, [
      [ "CombinedControl", "classLab07CombinedControl_1_1CombinedControl.html", "classLab07CombinedControl_1_1CombinedControl" ]
    ] ],
    [ "Lab07motordriver", null, [
      [ "MotorDriver", "classLab07motordriver_1_1MotorDriver.html", "classLab07motordriver_1_1MotorDriver" ]
    ] ],
    [ "Lab0x02", null, [
      [ "TaskLEDControl", "classLab0x02_1_1TaskLEDControl.html", "classLab0x02_1_1TaskLEDControl" ]
    ] ],
    [ "Lab0x02task2virtual", null, [
      [ "TaskLEDControlV", "classLab0x02task2virtual_1_1TaskLEDControlV.html", "classLab0x02task2virtual_1_1TaskLEDControlV" ]
    ] ],
    [ "lab4UI", null, [
      [ "UserInterface", "classlab4UI_1_1UserInterface.html", "classlab4UI_1_1UserInterface" ]
    ] ],
    [ "lab6closedloop", null, [
      [ "ClosedLoop", "classlab6closedloop_1_1ClosedLoop.html", "classlab6closedloop_1_1ClosedLoop" ]
    ] ],
    [ "lab6Encoder", null, [
      [ "Encoder", "classlab6Encoder_1_1Encoder.html", "classlab6Encoder_1_1Encoder" ]
    ] ],
    [ "Lab6ExecTask", null, [
      [ "Lab06ExecTask", "classLab6ExecTask_1_1Lab06ExecTask.html", "classLab6ExecTask_1_1Lab06ExecTask" ]
    ] ],
    [ "Lab6ExecTaskTEST", null, [
      [ "Lab06ExecTask", "classLab6ExecTaskTEST_1_1Lab06ExecTask.html", "classLab6ExecTaskTEST_1_1Lab06ExecTask" ]
    ] ],
    [ "lab6PCUI", null, [
      [ "UserInterface", "classlab6PCUI_1_1UserInterface.html", "classlab6PCUI_1_1UserInterface" ]
    ] ],
    [ "Lab6TaskControl", null, [
      [ "Lab06Control", "classLab6TaskControl_1_1Lab06Control.html", "classLab6TaskControl_1_1Lab06Control" ]
    ] ],
    [ "lab7closedloop", null, [
      [ "ClosedLoop", "classlab7closedloop_1_1ClosedLoop.html", "classlab7closedloop_1_1ClosedLoop" ]
    ] ],
    [ "lab7Encoder", null, [
      [ "Encoder", "classlab7Encoder_1_1Encoder.html", "classlab7Encoder_1_1Encoder" ]
    ] ],
    [ "lab7PCUI", null, [
      [ "UserInterface", "classlab7PCUI_1_1UserInterface.html", "classlab7PCUI_1_1UserInterface" ]
    ] ]
];