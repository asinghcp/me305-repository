var classlab7PCUI_1_1UserInterface =
[
    [ "__init__", "classlab7PCUI_1_1UserInterface.html#a306f9e988a5a74d5b5aee92d8006d44f", null ],
    [ "run", "classlab7PCUI_1_1UserInterface.html#ad1d4472de8705e9f0445d6adf166be66", null ],
    [ "transitionTo", "classlab7PCUI_1_1UserInterface.html#a9efa0fadab8982904e7a07da0f8234f2", null ],
    [ "curr_time", "classlab7PCUI_1_1UserInterface.html#a3621ccf59cbf6846d18b2f345533ee8a", null ],
    [ "interval", "classlab7PCUI_1_1UserInterface.html#ad40a11c20ca2c414d16dc81818eb60d7", null ],
    [ "Kp", "classlab7PCUI_1_1UserInterface.html#ab13e801a20fd668c21f3b82f28e1a3dd", null ],
    [ "next_time", "classlab7PCUI_1_1UserInterface.html#aa96f7681917602be0d0c5d01a05f3c5f", null ],
    [ "ser", "classlab7PCUI_1_1UserInterface.html#acb44a5098e1c20f7756f3cd0fb905046", null ],
    [ "start_time", "classlab7PCUI_1_1UserInterface.html#a95b748c86f028fbc2b5b7be87421881d", null ],
    [ "state", "classlab7PCUI_1_1UserInterface.html#a5dff9350015dcc4ed7d1638e53424d1a", null ],
    [ "t_ref", "classlab7PCUI_1_1UserInterface.html#aa0ebe6f0dae9fd367f623ad7278f6eb3", null ],
    [ "t_trunc", "classlab7PCUI_1_1UserInterface.html#a1f04cbbb92b2f73fed4273ab1a44352f", null ],
    [ "v_ref", "classlab7PCUI_1_1UserInterface.html#a66fc03732986544fc60b4eeac14ec30d", null ],
    [ "v_trunc", "classlab7PCUI_1_1UserInterface.html#a9860a79ba548242efcbe3ee5c055217e", null ],
    [ "x_ref", "classlab7PCUI_1_1UserInterface.html#a67bab35f6476ef23a611f6f442fe1c2d", null ],
    [ "x_trunc", "classlab7PCUI_1_1UserInterface.html#afebe2da33f38f481fc1b41e6fc31c5b9", null ]
];