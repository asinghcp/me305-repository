var classlab7closedloop_1_1ClosedLoop =
[
    [ "__init__", "classlab7closedloop_1_1ClosedLoop.html#ac899050f90bccfa5930fe4399e85a5dd", null ],
    [ "get_Kp", "classlab7closedloop_1_1ClosedLoop.html#ab2c49c0cdab732f540605da8caaa39d5", null ],
    [ "set_Kp", "classlab7closedloop_1_1ClosedLoop.html#a152833ee722845fe7fe8aa8a97372cc6", null ],
    [ "update", "classlab7closedloop_1_1ClosedLoop.html#aa85c0187c1d8d5ff7def4edbe879fd0f", null ],
    [ "Kp", "classlab7closedloop_1_1ClosedLoop.html#a51f5707cf6181b80e473ebb2912b4f59", null ],
    [ "max_value", "classlab7closedloop_1_1ClosedLoop.html#aa8ff5ee8bc80710ae0f6298e05b779e9", null ],
    [ "min_value", "classlab7closedloop_1_1ClosedLoop.html#a376ae62086202e1f2339e68b08f474e3", null ]
];