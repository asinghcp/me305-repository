var files_dup =
[
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Homework0x00.py", "Homework0x00_8py.html", [
      [ "TaskElevator", "classHomework0x00_1_1TaskElevator.html", "classHomework0x00_1_1TaskElevator" ],
      [ "Button", "classHomework0x00_1_1Button.html", "classHomework0x00_1_1Button" ],
      [ "MotorDriver", "classHomework0x00_1_1MotorDriver.html", "classHomework0x00_1_1MotorDriver" ]
    ] ],
    [ "Lab02main.py", "Lab02main_8py.html", "Lab02main_8py" ],
    [ "Lab02Physical.py", "Lab02Physical_8py.html", [
      [ "TaskLEDControlPhysical", "classLab02Physical_1_1TaskLEDControlPhysical.html", "classLab02Physical_1_1TaskLEDControlPhysical" ]
    ] ],
    [ "Lab03encoder.py", "Lab03encoder_8py.html", [
      [ "TaskEncoder", "classLab03encoder_1_1TaskEncoder.html", "classLab03encoder_1_1TaskEncoder" ]
    ] ],
    [ "Lab03main.py", "Lab03main_8py.html", "Lab03main_8py" ],
    [ "Lab03shares.py", "Lab03shares_8py.html", "Lab03shares_8py" ],
    [ "Lab03user.py", "Lab03user_8py.html", [
      [ "TaskUser", "classLab03user_1_1TaskUser.html", "classLab03user_1_1TaskUser" ]
    ] ],
    [ "Lab04encoder.py", "Lab04encoder_8py.html", [
      [ "Lab4TaskEncoder", "classLab04encoder_1_1Lab4TaskEncoder.html", "classLab04encoder_1_1Lab4TaskEncoder" ]
    ] ],
    [ "Lab04UImain.py", "Lab04UImain_8py.html", "Lab04UImain_8py" ],
    [ "Lab06CombinedControl.py", "Lab06CombinedControl_8py.html", [
      [ "CombinedControl", "classLab06CombinedControl_1_1CombinedControl.html", "classLab06CombinedControl_1_1CombinedControl" ]
    ] ],
    [ "Lab06motordriver.py", "Lab06motordriver_8py.html", "Lab06motordriver_8py" ],
    [ "Lab06shares.py", "Lab06shares_8py.html", "Lab06shares_8py" ],
    [ "Lab06UImain.py", "Lab06UImain_8py.html", "Lab06UImain_8py" ],
    [ "Lab07CombinedControl.py", "Lab07CombinedControl_8py.html", [
      [ "CombinedControl", "classLab07CombinedControl_1_1CombinedControl.html", "classLab07CombinedControl_1_1CombinedControl" ]
    ] ],
    [ "Lab07motordriver.py", "Lab07motordriver_8py.html", "Lab07motordriver_8py" ],
    [ "Lab07UImain.py", "Lab07UImain_8py.html", "Lab07UImain_8py" ],
    [ "Lab0x02.py", "Lab0x02_8py.html", [
      [ "TaskLEDControl", "classLab0x02_1_1TaskLEDControl.html", "classLab0x02_1_1TaskLEDControl" ]
    ] ],
    [ "Lab0x02task2virtual.py", "Lab0x02task2virtual_8py.html", [
      [ "TaskLEDControlV", "classLab0x02task2virtual_1_1TaskLEDControlV.html", "classLab0x02task2virtual_1_1TaskLEDControlV" ]
    ] ],
    [ "Lab1fib.py", "Lab1fib_8py.html", "Lab1fib_8py" ],
    [ "lab4UI.py", "lab4UI_8py.html", "lab4UI_8py" ],
    [ "lab6closedloop.py", "lab6closedloop_8py.html", [
      [ "ClosedLoop", "classlab6closedloop_1_1ClosedLoop.html", "classlab6closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab6ExecTask.py", "Lab6ExecTask_8py.html", [
      [ "Lab06ExecTask", "classLab6ExecTask_1_1Lab06ExecTask.html", "classLab6ExecTask_1_1Lab06ExecTask" ]
    ] ],
    [ "Lab6ExecTaskTEST.py", "Lab6ExecTaskTEST_8py.html", [
      [ "Lab06ExecTask", "classLab6ExecTaskTEST_1_1Lab06ExecTask.html", "classLab6ExecTaskTEST_1_1Lab06ExecTask" ]
    ] ],
    [ "lab6PCUI.py", "lab6PCUI_8py.html", "lab6PCUI_8py" ],
    [ "Lab6TaskControl.py", "Lab6TaskControl_8py.html", [
      [ "Lab06Control", "classLab6TaskControl_1_1Lab06Control.html", "classLab6TaskControl_1_1Lab06Control" ]
    ] ],
    [ "lab7closedloop.py", "lab7closedloop_8py.html", [
      [ "ClosedLoop", "classlab7closedloop_1_1ClosedLoop.html", "classlab7closedloop_1_1ClosedLoop" ]
    ] ],
    [ "lab7Encoder.py", "lab7Encoder_8py.html", [
      [ "Encoder", "classlab7Encoder_1_1Encoder.html", "classlab7Encoder_1_1Encoder" ]
    ] ],
    [ "lab7PCUI.py", "lab7PCUI_8py.html", "lab7PCUI_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_Homework0x00.py", "main__Homework0x00_8py.html", "main__Homework0x00_8py" ],
    [ "mainTEST.py", "mainTEST_8py.html", "mainTEST_8py" ]
];