var classlab6closedloop_1_1ClosedLoop =
[
    [ "__init__", "classlab6closedloop_1_1ClosedLoop.html#a04fca9c95e40fbbfb5b8af4360ebc0a9", null ],
    [ "get_Kp", "classlab6closedloop_1_1ClosedLoop.html#a8e3fed7e1902037bdd5ab9c076114d4b", null ],
    [ "set_Kp", "classlab6closedloop_1_1ClosedLoop.html#a939583c701a68440d90c20104d2a5077", null ],
    [ "update", "classlab6closedloop_1_1ClosedLoop.html#ad9fbc8c9beef0771a014e70a5603c1ba", null ],
    [ "Kp", "classlab6closedloop_1_1ClosedLoop.html#a230557d0744c31cf5dcd9c4d8a2e2ab8", null ],
    [ "max_value", "classlab6closedloop_1_1ClosedLoop.html#a00ad805101a967d8356ebdbec5579b92", null ],
    [ "min_value", "classlab6closedloop_1_1ClosedLoop.html#abd933f231b2c6fd39e9d76f30f80f911", null ]
];