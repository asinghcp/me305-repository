'''
@file lab4UI.py
@author Anil Singh
@brief This file contains the Spyder User Interface to read data from a Nucleo
@details This file contains the Spyder User Interface to read data from a
         Nucleo. The user can press 'g' to start data collection, and press
         's' to stop_collection data collection. This iteraction utilizes the pySerial
         module in the UI and the UART module within the Nucleo. Data is 
         compiled into a .csv file and plotted.
'''

import serial
import time
import csv
import numpy
from matplotlib import pyplot as pyb

ser = serial.Serial(port='COM6',baudrate=115273,timeout=1)
#sets baudrate, COM port, and timeout


class UserInterface:
    '''
    User interface task.
    
    An object of this class runs the UI. User inputs include 'g' to start
    data collection, and 's' to stop_collection data collection. 
    '''

    ## Initialization state
    S0_INIT          = 0
    
    ## Waitign for input state
    S1_CollectData  = 1
    

    def __init__(self, interval):
        '''
        Creates a UI task object.
        @param tasknum Number specifying the task
        @param dbg Indicates if task should print trace
        @param interval Microseconds between runs of task
        '''
    
        ##  Port
        self.ser = ser
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        print('''
              Data collection controls for Nucleo:
              User Inputs:
                  g: Start data collection
                  s: Stop data collection
              Auto-shutoff of data collection occurs after 10 seconds 
              without any user input.
              ''')
              
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 code
                
                self.cmd = input('Press g to begin data collection: ')
                if self.cmd == 'g':
                    self.ser.write(str(self.cmd).encode('ascii'))
                    self.cmd = None
                    self.transitionTo(self.S1_CollectData)
                    collectionpoint = time.time()
                    
                elif self.cmd != 'g':
                    print('Input valid entry [g]')
                    
                else:
                    # Invalid state code (error handling)
                    pass
                
            elif(self.state == self.S1_CollectData):
                # Run State 0 code
                self.stop_collection = input('Press s to stop data collection: ')
                
                if self.stop_collection == 's' or time.time - collectionpoint >= 10:
                    
                    if self.stop_collection == 's':
                        self.ser.write(str(self.stop_collection).encode('ascii'))
                        self.stop_collection = None
                    
                    # Read Data
                    data_list = list(ser.readline().decode('ascii').split('MM'))
                    data = [m.strip('[]').split(', ') for m in data_list]
                    timestamp = [int(t) for t in data[0]]
                    enc_pos = [int(t) for t in data[1]]
                    
                    
                    # CSV File Creation
                    filename = 'Encoder Output' + time.strftime('%Y%m%d-%H%M%S')
                    numpy.savetxt(filename, [timestamp, enc_pos], delimiter = ', ')
                    quit()
                    
                    # Plotting
                    pyb.plot(timestamp, enc_pos)
                    pyb.xlabel('Time (s)')
                    pyb.ylabel('Encoder Position')
                    
                elif(self.term != 's'):
                    self.cmd = print('Input valid entry')
                    
                else:
                    # Invalid state code (error handling)
                    pass
                
            # Specifying the next time the task will run
            self.next_time += self.interval

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState New state object assumes
        '''
        self.state = newState