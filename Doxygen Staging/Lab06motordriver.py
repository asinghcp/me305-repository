'''
@file Lab06motordriver.py
@author Anil Singh
@brief Class to run a Motor Driver
'''
import pyb
class MotorDriver:
    ''' This class implements a motor driver for the ME405 board.'''
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''Creates a motor driver by initializing GPIO pins and turning the 
        motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin    A pyb.Pin object to use as the input to half bridge 
                          1
        @param IN2_pin    A pyb.Pin object to use as the input to half bridge 
                          2
        @param timer      A pyb.Timer object to use for PWM generation on the 
                          IN1_pin and IN2_pin
        '''
        ## Initialize pins and motor
        print('Initializing Motor Driver')
        
        #input_pin  = 'pyb.Pin.cpu.' + IN1_pin ## Controls Pin1
        #output_pin = 'pyb.Pin.cpu.' + IN2_pin ## Controls Pin2
        #sleep_pin  = 'pyb.Pin.cpu.' + nSLEEP_pin ##Disables Motor
        
        self.pinA15 = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.pinA15.low() #ensures pin A15 always starts low
        pinB4 = pyb.Pin(IN1_pin)
        pinB5 = pyb.Pin(IN2_pin)
        timer = pyb.Timer(timer, freq=20000) ## Sets up timer
        self.ch1 = timer.channel(1, pyb.Timer.PWM, pin=pinB4)
        self.ch2 = timer.channel(2, pyb.Timer.PWM, pin=pinB5)
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)

        
    def enable(self):
        '''
        @brief            Enables motor by setting pin A15 high
        '''
        self.pinA15.high()
        print('Enabling Motor')
        
    def disable(self):
        '''
        @brief            Disables motor by setting pin A15 low
        '''
        self.pinA15.low()
        print('Disabling Motor')
        
    def set_duty(self, duty):
        '''
        @brief            Sets PWM output to motor
        @param duty       Value ranging in between -100 and +100 that controls the pulse width modulation percentage.
        '''
        if duty >= 0:
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(duty)
                        
        if duty < 0:
            self.ch1.pulse_width_percent(abs(duty))
            self.ch2.pulse_width_percent(0)                         

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = 'A15';
    pin_IN1 = 'B4';
    pin_IN2 = 'B5';

    # Create the timer object used for PWM generation
    tim = 3;

    # Create a motor object passing in the pins and timer
    mot = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    mot.enable()

    # User sets the duty cycle 5 different times, then the motor turns off
    n = 1
    while n <= 5:
        In = input('Please input desired motor speed (-100 to 100): ')
        mot.set_duty(int(In))
        n += 1
    
    mot.set_duty(0)
    mot.disable()       
