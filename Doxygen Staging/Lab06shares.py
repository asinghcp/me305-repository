'''
@file Lab06shares.py
@brief Passes variables between tasks for Lab 6
@author Anil Singh
'''

## reference velocity
ref_vel = None

## Kp
#Kp = None

## Initilization Variable
init = None

## command
cmd = None

## Empty data arrays
ref = [] ## Reference velocity
time = [] ## Time
velocity = [] ## Velocity
