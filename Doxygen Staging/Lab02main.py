'''
@file Lab02main.py

This file runs the finite state machine for the blinking LED & pulsing LED.

@author Anil Singh
'''

#from Lab0x02 import TaskLEDControl
#from Lab02Physical import TaskLEDControlPhysical
from Lab0x02task2virtual import TaskLEDControlV

# Creating a task object
#task1 = TaskLEDControl(1) #Sets period
#task2 = TaskLEDControlPhysical(1) #Sets period
task3 = TaskLEDControlV(0.01) #Sets period

# Run the tasks in sequence over and over again
while(True):
   #task1.run()
    #task2.run()
    task3.run()
