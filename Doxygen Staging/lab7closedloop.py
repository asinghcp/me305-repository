'''
@file lab7closedloop.py
@author Anil Singh
@brief Class to run, manage, and format data from an encoder to a motor driver
'''

class ClosedLoop:
    '''
    @brief A finite state machine that runs, manages, and formats data from an encoder to a motor driver.
    @details This class takes user and data input from the user and encoder,
             and outputs data to the motor driver.
    '''  
    def __init__(self, Kp, max_value, min_value):
        '''
        @brief       Creates a closed loop object
        '''
        self.Kp = Kp
        self.max_value = max_value
        self.min_value = min_value
 
    def update(self, Kp_x, ref_value, meas_value, ref_pos, meas_pos):
        '''
        @brief         Given a single reference value and measured value, computes and returns actuation value
        @param meas_value The measured value from the encoder
        '''
        
        ## Positional Kp_X
        
        
        ## Proportional controller algebra
        delta_val = ref_value - meas_value
        #delta_val = ref_value
        L = self.Kp*delta_val + Kp_x*(ref_pos-meas_pos)
        
        ## Forces PWM signal to stay between min and max range (in this case
        ## between -100 and +100) if a vaue exceeding the limits is called
        
        if L < self.min_value:
            L = self.min_value
        elif L > self.max_value:
            L = self.max_value
        return L
    
    def set_Kp(self, new_value):
        '''
        @brief          Sets the value of Kp to the new value
        '''
        self.Kp = new_value
        
    def get_Kp(self):
        '''
        @brief          Returns the current value of Kp
        '''
        return self.Kp
    
