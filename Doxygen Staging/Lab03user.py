'''
@file Lab03user.py
@brief Task to output user input for Lab 3
@author Anil Singh
'''

import Lab03shares
import utime
from pyb import UART

class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with the serial port UART2, waiting for 
    user input.   
    '''

    ## Initialization state
    S0_INIT          = 0
    
    ## Waitign for input state
    S1_WAIT_INPUT    = 1
    
    ## Waiting for response state
    S2_WAIT_RESP     = 2

    def __init__(self, tasknum, interval, dbg=True):
        '''
        Creates a UI task object.
        @param tasknum Number specifying the task
        @param dbg Indicates if task should print trace
        @param interval Microseconds between runs of task
        '''
    
        ##  Number identifying the task
        self.tasknum = tasknum
        
        ##  The amount of time in seconds between runs of the task
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.serial = UART(2)
        
        if self.dbg:
            print('Created user interface task')
        
        print('''
              Encoder User Interface:
                  Press z to Zero Encoder Position
                  Press p to Print Current Encoder Position
                  Press d to Print updated Delta Value    
              ''')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_INPUT)
            
            elif(self.state == self.S1_WAIT_INPUT):
                self.printTrace()
                # Run State 1 Code
                if self.serial.any():
                    self.transitionTo(self.S2_WAIT_RESP)
                    Lab03shares.cmd = self.serial.readchar()
                    
            
            elif(self.state == self.S2_WAIT_RESP):
                self.printTrace()
                # Run State 2 Code
                if Lab03shares.resp:
                    self.transitionTo(self.S1_WAIT_INPUT)
                    print(Lab03shares.printval)
                    Lab03shares.printval = None
                    Lab03shares.resp = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.tasknum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)