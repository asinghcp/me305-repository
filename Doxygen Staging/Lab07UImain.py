'''
@file Lab07UImain.py
@brief The main file running UI for Lab 7
@author Anil Singh
'''

import time
import serial
import numpy
from matplotlib import pyplot
from lab7PCUI import UserInterface

task = UserInterface(0.1) # Interval

## Run Task
while True:
    task.run()

 