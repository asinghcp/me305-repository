'''
@file    Lab06CombinedControl.py
@brief   This file contains the combined Execution and Control Tasks for Lab 6
@details This file combines the previously separated ExecTask and TaskControl
         due to an inability to solve an encoding issue. The purpose of this
         combination is to bypass a shares file and implement everything in
         one file.
@author  Anil Singh
'''
## From TaskControl:
import pyb
#import Lab06shares
import utime
from pyb import UART

## From ExecTask:
#import pyb
#import utime
#import Lab06shares
from lab6Encoder import Encoder
from Lab06motordriver import MotorDriver
from lab6closedloop import ClosedLoop

class CombinedControl:
    
    ## Initialization state
    S0_INIT          = 0
    
    ## Control state
    S1_Control       = 1 

    ## Finalization State
    S2_Final         = 2
    
    def __init__(self,interval):
        '''
        This class coordinates the inputs from \ref lab6PCUI.py to control the 
        motor system through the Nucleo. The class calls the closed loop file and 
        encoder data, and sends the PWM control scheme as an output.
        '''
        ##  The amount of time in microseconds between runs of the task
        self.interval = interval
        
        ## Timing 
        ##  Serial Port communication
        self.serial = UART(2)
        ## first iteration
        self.start_time = utime.ticks_us()
        ## next iteration timestamp
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
        ## Initial State
        self.state = self.S0_INIT
        
        ## Store copies of classes from imported modules
        self.Encoder = Encoder('B6', 'B7', 4) ## Encoder pins
        self.MotorDriver = MotorDriver('A15', 'B4', 'B5', 3) ## Motor Pins
        
        ## Enable Motor Driver
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        self.ClosedLoop = ClosedLoop
        ## Empty arrays from shares file:
        self.ref = []
        self.time = []
        self.velocity = []
    
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        #self.serial.write('Test'.encode('ascii'))
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            ## Update Encoder
            self.Encoder.update()
            
            if self.state == self.S0_INIT:
                
                if self.serial.any() != 0:
                ## Read data in serial port
                    self.Kp = float(self.serial.readline())
                    self.ref_vel = int(self.serial.readline())
                    #Lab06shares.Kp =self.Kp
                    #Lab06shares.vel_ref = self.vel_ref
                    
                    self.ClosedLoop = ClosedLoop(self.Kp, 100, -100) ## Kp and Max and Min PWM Limit
                    ## Transition to state 1
                    self.transitionTo(self.S1_Control)

            if self.state == self.S1_Control:
                ## Check for stop
                if self.serial.any() == 0:
                 
                    
                    ## Read Encoder Value, convert ro revolutions per minute (RPM)
                    ## 4*1000CPR
                    PPR = 4000
                    interval_change = self.Encoder.get_delta()/PPR
                    divider1 = self.interval*1E-6
                    divider2 = divider1/60
                    velocity_actual = interval_change/divider2
                    
                    ## Write/append data to Lab06shares
                    self.velocity.append(velocity_actual)                    
                    self.time.append((self.curr_time - self.start_time)/1E6)
                    self.ref.append(self.ref_vel)
                    
                    ## Control Loop Gain Algebra
                    L = self.ClosedLoop.update(self.ref_vel, velocity_actual)
                    velocity_actual = None
                    
                    ## Set Motor Driver Output to L
                    self.MotorDriver.set_duty(L)  
                    
                else:
                    ## Duty Cycle set to 0 for all other cases other than desired
                    self.MotorDriver.set_duty(0)
                    self.serial.write(str(self.time)+'MM'+str(self.velocity)+'MM'+str(self.ref))
                    self.transitionTo(self.S2_Final)
                    
            if self.state == self.S2_Final:
                pass
                    
            else:
                pass
                
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    