'''
@file main_Homework0x00.py

This file runs the finite state machine for the Elevator
'''

from Homework0x00 import Button, MotorDriver, TaskElevator


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
button_1 = Button('PB6')
button_2 = Button('PB7')
first = Button('PB8')
second = Button('PB9')
motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, button_1, button_2, first, second, motor)
task2 = TaskElevator(0.1, button_1, button_2, first, second, motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()