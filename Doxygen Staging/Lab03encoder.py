'''
@file Lab03encoder.py
@brief Task to output encoder output
@author Anil Singh
'''

import Lab03shares
import utime
from Encoder import Encoder

class TaskEncoder:
    '''
    Encoder task to run Lab03.py
    
    Objects in this class read and interpret values from the encoder, directed by a UI 
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0    
    
    ## Constant waiting for command from UI
    S1_WAIT_CMD           = 1    
      
     
    def __init__(self, tasknum, interval, pin1, pin2, timer, Encoder, dbg=True):
        '''
        @brief Creates a TaskEncoder object.
        @param interval Microseconds between runs of task
        @param pin1 First Pin on Nucleo L476RG encoder is connected to
        @param pin2 Second Pin on Nucleo L476RG encoder is connected to
        @param timer Timer corresponding to appropriate pins
        @param Encoder Encoder Class
        @param dbg Indicates if task should print trace
        @param tasknum Number identifying the task
        '''
        ##  Number identifying the task
        self.tasknum = tasknum
        
        ##  The amount of time in seconds between runs of the task
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created encoder task')
        
        ## Class copy
        self.Encoder = Encoder
        
        ## initialize encoder position
        self.enc_pos = 0
        
        ## initialize encoder prev value
        self.enc_prev = 0
        
        ## timing
        self.tim = Encoder.tim
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''

        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            Encoder.update(self)
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code           
                self.printTrace()
                self.transitionTo(self.S1_WAIT_CMD)
               
            elif(self.state == self.S1_WAIT_CMD):
                # Run State 1 Code                
                self.printTrace()                
                if Lab03shares.cmd == 112:
                    Lab03shares.cmd = None
                    Lab03shares.printval = int(Encoder.get_position(self))
                    Lab03shares.resp = 1
                    
                elif Lab03shares.cmd == 122:
                    Lab03shares.cmd = None
                    Lab03shares.printval = 'Encoder Zeroed'                    
                    self.Encoder.set_position(0) 
                    Lab03shares.resp = 1
                    
                elif Lab03shares.cmd == 100:                    
                    Lab03shares.cmd = None
                    Lab03shares.printval = int(Encoder.get_delta(self))
                    Lab03shares.resp = 1
                    
                else:
                    Lab03shares.printval = 'Input a valid command'
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifies next time task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new object state.
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
