'''
@file lab6PCUI.py
@author Anil Singh
@brief This file contains the Spyder User Interface to read data from a Nucleo
@details This file contains the Spyder User Interface to read data from a
         Nucleo. The user can press input a Gain value and the motor will 
         spin accordingly. The velocity response is then plotted. 
'''

import serial as cereal
import time as thyme
# import numpy as jacob
from matplotlib import pyplot as kai

ser = cereal.Serial(port='COM6',baudrate=115273,timeout=1)
#sets baudrate, COM port, and timeout


class UserInterface:
    '''
    User interface task.
    
    An object of this class runs the UI. User inputs desired proportionality
    constant for the closed loop.
    '''
    ## Initialization state
    S0_INIT          = 0
    
    ## Data Processing state
    S1_Data          = 1
    
    ## Finalization State
    S2_Final         = 2

    def __init__(self, interval, ref_vel):
        '''
        Creates a UI task object.
        @param interval Microseconds between runs of task
        '''
    
        ##  Port
        self.ser = ser       
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        #self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = thyme.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Control Reference Velocity
        self.ref_vel = ref_vel
        
        
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = thyme.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                ## Run State 0 code
                Kp_input = input('Input Gain Value Kp: ') 
                self.kp_trans = int(float(Kp_input)*1E6)/1E6                    
                ## Write parameters to serial port
                ref_vel = str(self.ref_vel)
                kp_trans = str(self.kp_trans)+'\n'
                self.ser.write(kp_trans.encode())
                self.ser.write(ref_vel.encode())
                
                ## Transition to state 1
                self.transitionTo(self.S1_Data)
            
            ## Read and Plot Data
            if(self.state == self.S1_Data):
                ## Run state 1 code
                
                self.stop = input('To stop system enter s: ')
                
                ## Override system and terminate
                if self.stop == 's':
                    
                    ## Write stop command as 's' to nucleo
                    self.ser.write(str(self.stop).encode('ascii'))
                                                         
                    ## Clear                    
                    self.stop = None
            
                    # Read Data
                    data_list = list(ser.readline().decode('ascii').split('MM'))
                    data = [m.strip().strip('[]').split(', ') for m in data_list]
                    #print(data)
                    timestamp      = [float(t) for t in data[0]]
                    motor_velocity = [float(t) for t in data[1]]
                    velocity_input = [float(t) for t in data[2]]
                    
                    rel_time = [t - timestamp[0] for t in timestamp]
                    
                    
                    ## CSV File Creation
                    ## filename = 'Motor_Velocity'+thyme.strftime('%Y%m%d-%H%M%S')
                    ## jacob.savetxt(filename, [timestamp, motor_velocity], delimiter = ', ')
                    ## quit()
                    
                    ## Plotting
                    kai.plot(rel_time, motor_velocity, 'b', rel_time, velocity_input, 'g--')
                    ## Blue line for motor velocity, green dashed line for velocity input
                    kai.xlabel('Time (s)')
                    kai.ylabel('Motor Velocity (rpm)')
                    kai.title('Motor Velocity vs Time: Kp = ' +str(self.kp_trans))
                    ## Data labeling
                    
                    self.transitionTo(self.S2_Final)
                
                ## Stop override error handling                      
                elif(self.stop != 's'):
                    self.cmd = print('Input valid entry')
                    
            elif(self.state == self.S2_Final):
                quit()
                    
            # Specifying the next time the task will run
            self.next_time += self.interval                    


    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState New state object assumes
        '''
        self.state = newState                                        