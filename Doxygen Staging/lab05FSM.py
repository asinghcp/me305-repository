'''
@file Lab05FSM.py
@author Anil Singh
@brief Task to run bluetooth LED control
@details This Finite State Machine waits for user input from the bluetooth 
         app. The User has three input options on their device. They can
         turn the LED on, off, and specificy a blinking frequency. 
'''

import utime
import pyb
from pyb import UART

class Lab5LEDControl:
    '''
    LED Control task
    
    Objects in this class control the frequency of a pulsing LED via Bluetooth
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0    
    
    ## Constant waiting for command from App
    S1_WAIT_CMD           = 1   
    
    ## Constant defining State 2 - LED ON
    S2_LED_ON             = 2
    
    ## Constant defining State 3 - LED ON
    S3_LED_OFF            = 3
      
     
    def __init__(self, interval):
        '''
        @brief Creates a TaskEncoder object.
        @param pinA5 LED Pin Startup
        '''
 
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0     
        
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
                
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
         ## LED Pin Startup
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
                       
        ## Serial setup
        self.serial = UART(3, 9600)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if self.state == self.S0_INIT:
                print('System Initialized')
                self.pinA5.low() #Start LED Off
                self.transitionTo(self.S1_WAIT_CMD)
                
            if self.state == self.S1_WAIT_CMD:
                if self.serial.any() != 0:
                    self.appdata = self.serial.read()
                    
                    if self.appdata == b'1500':
                        self.pinA5.high()
                        self.appdata = None
                        print('LED ON')
                        
                    elif self.appdata == b'1501':
                        self.pinA5.low()
                        self.appdata = None
                        print('LED OFF')
                        
                    else:
                        next_interval = 0
                        interval = int((1/int(self.appdata))*1E6)
                        print('LED pulsing at {} Hz'.format(self.appdata))
                        self.appdata = None
                       
                        while self.serial.any() == 0:
                            loc_time = utime.ticks_us()
                            if utime.ticks_diff(loc_time, next_interval) >= 0:
                                if self.state == self.S2_LED_ON:
                                    self.pinA5.high()
                                    next_interval = utime.ticks_add(loc_time, interval)
                                    self.transitionTo(self.S3_LED_OFF)
                                elif self.state == self.S3_LED_OFF:
                                    self.pinA5.low()
                                    next_interval = utime.ticks_add(loc_time, interval)
                                    self.transitionTo(self.S2_LED_ON)
                                    
                        self.transitionTo(self.S1_WAIT_CMD)
                                    
                    self.runs += 1
            
                    # Specifies next time task will run
                    self.next_time = utime.ticks_add(self.next_time, self.interval) 

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new object state.
        '''
        self.state = newState
