'''
@file mainTEST.py
@brief Nucleo executes this file automatically on startup without the need for 'execfile'
@details Calls the Lab6TaskControl, Lab6ExecTask, Motor Driver, Closed Loop,
         and Encoder, using the shares file to coordinate variable
         transmission between tasks.
@author Anil Singh
'''

import utime
import pyb
import Lab06shares
from pyb import UART
from Encoder import Encoder
from Lab06motordriver import MotorDriver
from lab6closedloop import ClosedLoop
from Lab6TaskControl import Lab06Control
from Lab6ExecTask import Lab06ExecTask

ControlTask = Lab06Control(2_0000)
ExecutionTask = Lab06ExecTask(2_0000)

taskList = [ExecutionTask]

while True:
    for task in taskList:
        task.run()