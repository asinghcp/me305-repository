'''
@file Lab04UImain.py
@author Anil Singh
@brief main file to coordinate User Interface for Lab 4
'''

import time
import serial
import csv
from matplotlib import pyplot as pyb
from lab4UI import UserInterface

task = UserInterface(1)

while True:
    task.run()
