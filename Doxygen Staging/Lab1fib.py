'''
@file       Lab1fib.py
@brief      Function to calculate a Fibonacci Number.
@details    This file containins a function that calculates the nth term of 
            the Fibbonaci Sequence.
@author     Anil Singh
            
'''
def fib(idx):
    ''' 
    @brief      fib(idx) is a function that calculates the nth term of the Fibbonaci Sequence. The nth term is specified by the index labeled idx.
    @details    This function requires that the user input a real, positive
                integer for a value of n (indexed by idx).
    @param      idx is an integer specifiying the nth term of the Fibonacci
                Sequence.
    '''
    while True:
        fib_0=0
        fib_1=1
        idx = int(idx)
        if idx < 0: # handles invalid index error for negative idx values
            print('Please input a real, positive integer')
            
        elif idx == 0: # returns 0th term of Fibbonaci Sequence
            print('Fibonacci number at ' 'index n={:}:'.format(idx))
            print(fib_0)
            
        elif idx == 1: # returns 1st term of Fibbonaci Sequence
            print('Fibonacci number at ' 'index n={:}:'.format(idx))
            print(fib_1)
        
        else: # returns nth term of Fibbonaci Sequence indexed by all other values 
              # of idx
            print('Fibonacci number at ' 'index n={:}:'.format(idx))
            for n in range(2,idx+1): #sets calculation range
                # calculates fibonnaci number iteratively instead of recursively
                # using a recursive method vs an iterative method resulted in an
                # excessively long run time for large values of idx.
                fib_n = fib_0+fib_1 
                fib_0=fib_1
                fib_1=fib_n
            
            # Prints fibbonaci number
            print(fib_n)
        # Option for another index    
        idx=input('Enter another index: ')
                
if __name__ == '__main__': 
    fib(100)
