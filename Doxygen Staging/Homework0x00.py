'''
@file Homework0x00.py

This file serves as an implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator
operating between two floors.

The user has two buttons to call the evelator to a desired floor.

There is also a proximity sensor at either end of travel for the elevator.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                   = 0    
    
    ## Constant defining State 1
    S1_moving_down            = 1    
    
    ## Constant defining State 2
    S2_moving_up              = 2    
    
    ## Constant defining State 3
    S3_stopped_on_floor_1     = 3    
    
    ## Constant defining State 4
    S4_stopped_on_floor_2     = 4
    
    def __init__(self, interval, button_1, button_2, first, second, motor):
        '''
        @brief            Creates a TaskElevator object.
        @param button_1   An object from class Button representing first floor button
        @param button_2  An object from class Button representing the second floor button
        @param first An object from class Button representing the floor 1 sensor
        @param second An object from class Button representing the floor 2 sensor
        @param motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the first floor button
        self.button_1 = button_1
        
        ## The button object used for the second floor button
        self.button_2 = button_2
        
        ## The button object used for the the floor 1 sensor
        self.first = first
        
        ## The button object used for the the floor 2 sensor
        self.second = second
        
        ## The motor object representing a DC motor
        self.motor = motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                self.transitionTo(self.S1_moving_down)
                self.motor.down()
               
            elif(self.state == self.S1_moving_down):
                # Run State 1 Code
                if(self.first.getButtonState()):
                    self.transitionTo(self.S3_stopped_on_floor_1)
                    self.motor.stop()
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_moving_up):
                # Run State 2 Code
                 if(self.second.getButtonState()):
                    self.transitionTo(self.S4_stopped_on_floor_2)
                    self.motor.stop()
                 print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_stopped_on_floor_1):
                # Run State 3 Code
                if(self.button_2.getButtonState()):
                    self.transitionTo(self.S2_moving_up)
                    self.motor.up()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_stopped_on_floor_2):
                # Run State 4 Code
                if(self.button_1.getButtonState()):
                    self.transitionTo(self.S1_moving_down)
                    self.motor.down()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
        
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary user to call the elevator to a desired floor. As of 
                right now this class is implemented using "pseudo-hardware". 
                That is, we are not working with real hardware IO yet, this is 
                all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                move up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def up(self):
        '''
        @brief Moves the motor up
        '''
        print('Motor is moving up (motor = 1)')
    
    def down(self):
        '''
        @brief Moves the motor down
        '''
        print('Motor is moving up (motor = 2)')
    
    def stop(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Stopped (motor = 0)')



























