from lab6Encoder import Encoder
from Lab06motordriver import MotorDriver
Encoder = Encoder('B6', 'B7', 4)
pin_nSLEEP = 'A15';
pin_IN1 = 'B4';
pin_IN2 = 'B5';
tim = 3;
mot = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
mot.enable()
mot.set_duty(50)
while True:
    Encoder.update()
    print(str(Encoder.get_delta()))   
