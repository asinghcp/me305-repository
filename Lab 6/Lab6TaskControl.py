'''
@file Lab6TaskControl.py
@brief This task controls user input, and shares formatted data with the TaskCont
@author Anil Singh
'''

import pyb
import Lab06shares
import utime
from pyb import UART

class Lab06Control:
    '''
    This class communicates with the frontend User Interface, reciving user
    input and formatted data. The class then sends the data to TaskCont
    '''
    ## Initialization state
    S0_INIT          = 0
    
    ## Control state
    S1_Control       = 1    
    
    def __init__(self, interval):
        '''
        Creates a control task object.
        @param interval Microseconds between runs of task
        '''
        ##  The amount of time in microseconds between runs of the task
        self.interval = interval
        
        ## Timing
        ##  Serial Port communication
        self.serial = UART(2)
        ## first iteration
        self.start_time = utime.ticks_us()
        ## next iteration timestamp
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Initial State
        self.state = self.S0_INIT

    def run(self):
        
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Check for State 0
            if self.state == self.S0_INIT:
                
                    ## Check for data waiting in serial port
                    if self.serial.any() != 0:
                        ## Read data in serial port
                        #self.Kp = self.serial.readchar()
                        self.vel_ref = self.serial.read()
                        #Lab06shares.Kp =self.Kp
                        Lab06shares.vel_ref = self.vel_ref
                        ## Transition to state 1
                        self.transitionTo(self.S1_Control)
                        
            if self.state == self.S1_Control:
                ## Check for stop
                if self.serial.any() != 0:
                    self.cmd = self.serial.readchar()
                    if self.cmd == 115: ## ascii lowercase s
                        Lab06shares.cmd = True
                        self.serial.write(str(self.Lab06shares.time)+'MM'+str(self.Lab06shares.velocity)+'MM'+str(self.Lab06shares.ref))
                    else:
                        pass
            ## Invalid state code (error handling)
            else:
                pass
                                     
            #self.runs += 1

            ## Specifies next time task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)                        
                        
    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState New state object assumes
        '''
        self.state = newState                                        