'''
@file       main.py
@brief      Nucleo executes this file automatically on startup without the need for 'execfile'
@details    Calls the CombinedControl, Motor Driver, Closed Loop, and Encoder, 
            using the shares file to coordinate variable
            transmission between tasks.
@author     Anil Singh
'''

import utime
import pyb
#import Lab06shares
from pyb import UART
from lab6Encoder import Encoder
from Lab06motordriver import MotorDriver
from lab6closedloop import ClosedLoop
#from Lab6TaskControl import Lab06Control
#from Lab6ExecTask import Lab06ExecTask
from Lab06CombinedControl import CombinedControl

#ControlTask = Lab06Control(2_0000)
#ExecutionTask = Lab06ExecTask(2_0000)

task = CombinedControl(2_0000)

while True:
    task.run()