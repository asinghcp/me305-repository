'''
@file Lab6ExecTaskTEST.py
@brief This file contains the drivers needed for motor control, reading encoder data, and running  the closed loop
@author Anil Singh
'''

import pyb
import utime
import Lab06shares
from Encoder import Encoder
from Lab06motordriver import MotorDriver
from lab6closedloop import ClosedLoop

class Lab06ExecTask:
    '''
    This class coordinates the inputs from \ref lab6PCUI.py and 
    \ref Lab6TaskControl.py to control the motor system through the Nucleo.
    The class calls the closed loop file and encoder data, and sends the 
    PWM control scheme as an output.
    '''
    def __init__(self, interval):
        '''
        Creates a task to execute and coordinate tasks.
        '''
        ## Store copies of classes from imported modules
        self.Encoder = Encoder('B6', 'B7', 4) ## Encoder pins
        self.MotorDriver = MotorDriver('A15', 'B4', 'B5', 3) ## Motor Pins
        self.ClosedLoop = ClosedLoop(2.3, 100, -100) ## Kp and Min and Max PWM Limit
        
        ## Timing parameters
        self.interval = interval      
            ## interval, or, delta t for velocity calculation           
        self.start_time = utime.ticks_us()
            ## timestamp
        self.next_time = utime.ticks_add(self.start_time, self.interval)
            ## timestamp for next iteration of task
            
        ## Enable Motor Driver
        self.MotorDriver.enable()    
        
    def run(self):
        '''
        Runs one task iteration.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## Interprets encoder data and converts reading into revolutions per minute (rpm)
            if Lab06shares.init != None:
                ## Update Encoder
                self.Encoder.update()
                
                ## Read Encoder Value, convert ro revolutions per minute (RPM)
                ## 4*1000CPR
                PPR = 4000
                interval_change = self.Encoder.get_delta()/PPR
                divider1 = self.interval*10^6
                divider2 = divider1*1/60
                velocity_actual = interval_change/divider2
                
                ## Write/append data to Lab06shares
                #Lab06shares.velocity.append(velocity_actual)
                
                #Lab06shares.time.append(self.curr_time - self.start_time)
                #Lab06shares.ref.append(Lab06shares.ref_vel)
                
                ## Control Loop Gain Algebra
                L = self.ClosedLoop.update(100, velocity_actual)
                velocity_actual = None
                
                ## Set Motor Driver Output to L
                self.MotorDriver.set_duty(L)
                
                ## Specifies next time task will run
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                
            else:
                ## Duty Cycle set to 0 for all other cases other than desired
                self.MotorDriver.set_duty(0)
                
                
                

    