'''
@file Lab06UImain.py
@brief The main file running UI for Lab 6
@author Anil Singh
'''

import time
import serial
import numpy
from matplotlib import pyplot
from lab6PCUI import UserInterface

task = UserInterface(0.1, 500) # Interval, Kp, reference velocity

## Run Task
while True:
    task.run()

