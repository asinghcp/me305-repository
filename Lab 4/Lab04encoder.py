'''
@file Lab04encoder.py
@author Anil Singh
@brief Task to output encoder output
@details This task calls the Encoder class from Lab 3 and outputs the data 
         required to plot the position of the encoder over time. The data is
         processed here and sent to the User Interface to be plotted. This 
         task also relies on the UART module for serial communication and the 
         utime module for timing. 
'''

import utime
import pyb
from pyb import UART
from Encoder import Encoder

class Lab4TaskEncoder:
    '''
    Encoder task
    
    Objects in this class read and interpret values from the encoder, directed by a UI 
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0    
    
    ## Constant waiting for command from UI
    S1_WAIT_CMD           = 1    
      
     
    def __init__(self, interval, pin1, pin2, timer, Encoder):
        '''
        @brief Creates a TaskEncoder object.
        @param interval Microseconds between runs of task
        @param pin1 First Pin on Nucleo L476RG encoder is connected to
        @param pin2 Second Pin on Nucleo L476RG encoder is connected to
        @param timer Timer corresponding to appropriate pins
        @param Encoder Encoder Class
        '''
 
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
         ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
               
        ## Class copy
        self.Encoder = Encoder
        
        ## initialize encoder position at 0
        self.enc_pos = 0
        
        ## initialize encoder prev value at 0
        self.enc_prev = 0
        
        ## timing
        self.tim = pyb.Timer(3)
        
        ## Collect time data
        self.time = []
        
        ## Set timestamp
        self.timestamp = [] 
        
        ## Data interval
        self.data_interval = 0
        
        ## Collect position data
        self.collectdata = []
        
        ## Serial setup
        self.serial = UART(2)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        
        if self.serial.any() != 0:
            self.init = self.serial.readchar()
            
            if self.init == 103:
                self.init == None
                
                while True:
                    self.curr_time = utime.ticks_us()
                    
                    if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                        Encoder.update(self)
                        
                        if(self.state == self.S0_INIT):
                            # Run State 0 code
                            self.transitionTo(self.S1_WAIT_CMD)
                            
                        elif(self.state == self.S1_WAIT_CMD):
                            # Run State 1 code
                            
                            if self.serial.readchar() == 115 or utime.ticks_diff(self.curr_time, self.start_time) >= 10000000:
                                self.transitionTo(self.S0_INIT)
                                
                                for k in range(len(self.timestamp)):
                                    self.time.append((self.timestamp[k] - self.timestamp[0])/1E6)
                                    
                                self.cmd = None
                                self.serial.write(str(self.timestamp)+'MM'+str(self.collectdata)+'MM'+str(self.time))
                            
                            else:
                                Encoder.update(self)
                                self.timestamp.append(utime.ticks_diff(self.curr_time, self.start_time))
                                self.collectdata.append(Encoder.get_position(self))
                                    
                                
                        else:
                            pass
                                                 
                        self.runs += 1
            
                        # Specifies next time task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new object state.
        '''
        self.state = newState
