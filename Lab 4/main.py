'''
@file main.py
@author Anil Singh
@brief This file facilitates a software handshake between a UI frontend and a Nucleo
@details This file coordinates supporting files that collect data on a 
         Nucleo when instructed to do so be a UI interface in Spyder.
'''

import utime
from pyb import UART
from Encoder import Encoder
from Lab04encoder import Lab4TaskEncoder

Encoder = Encoder('A6', 'A7', 3)
task1 = Lab4TaskEncoder(2_00000, 'A6', 'A7', 3, Encoder)

while True:
    task1.run()

