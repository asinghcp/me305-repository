import matplotlib
import numpy

def ODE(xnot):
    xdotnot = 0
    k = 2
    alpha = 5
    
    t = [0]
    x = [xnot]
    xdot = [xdotnot]
    xdubdot = [-k*xnot-alpha*xnot*xnot*xnot]
    deltat = 0.1
    
    for i in range(1,100):
        t.append(t[i-1]+deltat)
        x.append(x[i-1]+xdot[i-1]*deltat+0.5*xdubdot[i-1]*deltat*deltat)
        xdubdot.append(-k*x[i]-alpha*x[i]*x[i]*x[i])
        xdot.append(xdot[i-1]+0.5*(xdubdot[i]+xdubdot[i-1])*deltat)
        
    matplotlib.pyplot.plot(t,x)
    matplotlib.pyplot.legend('Xnot = ' + str(xnot))

for n in range(1,10):
    ODE(n/10)
    
matplotlib.pyplot.xlabel('Time (s)')
matplotlib.pyplot.ylabel('Amplitude (units)')

        