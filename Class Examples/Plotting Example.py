# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 09:36:37 2020

@author: Anil
"""

import matplotlib.pyplot as plt
import numpy as np

time = np.array([0,1,2,3,4,5,6,7,8,9,10])
vals = time**2

plt.plot(time,vals,'ro-')
plt.xlabel('Time [s]')
plt.ylabel('Angle [deg]')
plt.grid(True)
plt.show()

np.savetxt('Section3.csv', vals, delimiter =',')
