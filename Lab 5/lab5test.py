import pyb
from pyb import UART

pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

uart = UART(3,9600)

while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val,' LED OFF')
            pinA5.low()
        elif val == 1:
            print(val,' LED ON')
            pinA5.high()
        else:
            print('Invalid Entry')
            val = None