'''
@file Lab03main.py
@brief Coordinates all encoder tasks for Lab 3.
@details This program synchronizes the UI Task, Encoder, Encoder Task,
         coordinated by a shares file. 
@author Anil Singh
'''

from Lab03user import TaskUser
from Lab03encoder import TaskEncoder
from Encoder import Encoder

Encoder = Encoder('A6','A7', 3)

## User interface task, works with serial port
task0 = TaskUser  (0, 1_00, dbg=False)

## Encoder Task interacts with Motor Encoder hardware to read and write position
task1 = TaskEncoder(1, 1_00, 'A6', 'A7', 3, Encoder, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()
