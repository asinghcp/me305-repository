'''
@file Lab03shares.py
@brief Passes variables between tasks for Lab 3
@author Anil Singh
'''

## Command sent from UI to Encoder
cmd = None

## Response from Encoder to UI
resp = None

## Printed Value
printval = None