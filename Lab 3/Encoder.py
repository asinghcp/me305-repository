'''
@file Encoder.py
@author Anil Singh

@brief This file serves as an encoder class for interacting with a motor for
encoder using a Nucleo L476RG Board (For Lab 3 & 4). This Encoder class has
been updated for use in Lab 6 and 7, and is only included for reference. 



'''
import pyb

class Encoder:
    '''
    @brief A finite state machine that reads data from the motor encoder.
    @details This class takes user and data input from an encoder, handles
                under and overflow, and determines position. 
    '''  
    def __init__(self, pin1, pin2, timer):
        '''
        @brief       Creates an encoder associated with a pin and timer
        @param pin1 This variable contains the string corresponding to the first pin.
        @param pin2 This variable contains the string corresponding to the second pin.     
        '''
        
        tim = pyb.Timer(timer)
        tim.init(prescaler=0, period=0xFFFF)        
        pinA = pyb.Pin(pin1)
        pinB = pyb.Pin(pin2)
        tim.channel(1, pin=pinA, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=pinB, mode=pyb.Timer.ENC_AB)
        
        self.enc_pos = 0
        self.enc_prev = 0
        self.tim = tim
        self.offset = 0
        
        
    def update(self):
        '''
        @brief         Updates encoder position and records encoder's previous position'
        @param enc_pos Variable containing updated encoder position
        '''
        self.enc_prev = self.enc_pos
        self.enc_pos = self.tim.counter() - self.offset
        self.delta = self.enc_pos - self.enc_prev
        
    def get_position(self):
        '''
        @brief          Returns the value of the encoder
        @param enc_pos Variable containing updated encoder position
        '''
        return(self.enc_pos)
    
    def set_position(self, new_pos):
        '''
        @brief          Sets the value of the encoder to new_pos
        @param new_pos Variable containing new encoder position
        '''
        self.offset = self.enc_pos
        self.enc_pos = new_pos
        #print(self.enc_pos)
        
    def get_delta(self):
        '''
        @brief          Returns delta (difference between updates)
        @param enc_pos Variable containing updated encoder position
        @param enc_prev Variable containing previous encoder position
        '''
        if abs(self.delta) >= 2^15:
            if self.delta < 0:
                self.delta += 2^16
            elif self.delta > 0:
                self.delta = self.delta - 2^16
        return int(self.delta)